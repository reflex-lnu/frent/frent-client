import './footer.css'
import {Link} from "react-router-dom";

const Footer = () => {
  return (
    <footer className='footer'>
      <ul>
        <li>
          <Link to='https://reflex-lnu.gitlab.io/frent/frent-docs/'><p>Documentation</p></Link>
        </li>
        <li>
          <p>Copyright © 2022 reflex</p>
        </li>
      </ul>
    </footer>
  );
};

export default Footer;
