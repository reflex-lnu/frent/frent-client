import './table.css'

const Table = ({schema, data}) => {
  return (
    <table className='table'>
      <thead>
      <tr>
        {schema.map(c => <th key={c.name}>{c.label}</th>)}
      </tr>
      </thead>
      <tbody>
        {data.map(row => <tr>{schema.map(column => <td>{column.format(row[column.name])}</td>)}</tr>)}
      </tbody>
    </table>
  )
};

export default Table;
