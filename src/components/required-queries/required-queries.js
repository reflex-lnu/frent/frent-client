import {useState} from "react";
import Select from "react-select";
import Table from "./table";
import {requiredQuery} from "../../api";
import './required-queries.css'

const camel2scream = (camelCase) => camelCase
  .replace(/([A-Z])/g, (match) => ` ${match}`)
  .toUpperCase()
  .trim();

const completeParam = ([name, typeOrDef]) => [
  name,
  typeof typeOrDef === 'string'
    ? {name: name, type: typeOrDef, placeholder: name.toUpperCase()}
    : {name: name, ...typeOrDef}
];

const paramsDef = Object.fromEntries(Object.entries({
  n: 'number',
  a: 'number',
  b: 'number',
  clientId: 'number',
  friendId: 'number',
  from: 'datetime-local',
  to: 'datetime-local'
}).map(completeParam));

const identity = x => x;

const completeSchema = namesOrDefs => namesOrDefs.map(nameOrDef => {
  const def = (typeof nameOrDef === 'string'
    ? {name: nameOrDef}
    : nameOrDef);

  return {label: camel2scream(def.name), format: identity, ...def}
})

const RQsDef = [
  {
    title: 'For client C find all friends he rented at least N times for given period from F to T',
    params: ['clientId', 'n'],
    schema: ['friendId', 'friendFullName', 'partyStartsAt', 'timesRented']
  },
  {
    title: 'For friend X find all clients who rented him at least N times for given period from F to T',
    params: ['friendId', 'n'],
    schema: ['clientId', 'clientFullName', 'partyStartsAt', 'timesRented']
  },
  {
    title: 'For friend X find all events he was rented for at least N times for given period from F to T',
    params: ['friendId', 'n'],
    schema: ['eventId', 'eventTitle', 'timesRented']
  },
  {
    title: 'Find all clients who rented at least N distinct friends for given period from F to T',
    params: ['n'],
    schema: ['clientId', 'clientFullName', 'partyStartsAt', 'timesRented']
  },
  {
    title: 'Find all friends being rented at least N times for given period from F to T',
    params: ['n'],
    schema: ['friendId', 'friendFullName', 'partyStartsAt', 'timesRented']
  },
  {
    title: 'Find total number of parties per month',
    params: [],
    period: false,
    schema: ['month', 'parties']
  },
  {
    title: 'For friend X and each event he was rented for find how many times he was rented for this event in a group of at least N friends for given period from F to T',
    params: ['friendId', 'n'],
    schema: ['eventId', 'eventTitle', 'timesRented']
  },
  {
    title: 'Display the gifts in descending order of the amount of vacations taken by friends who received a gift from the client C for given period from F to T',
    params: ['clientId'],
    schema: ['giftId', 'giftDescription', 'vacations']
  },
  {
    title: 'Display friends in descending order of amount of complaints with at least N signs for given period from F to T',
    params: ['n'],
    schema: ['friendId', 'friendFullName', 'complaints']
  },
  {
    title: 'Find all common parties for the client C and friend X for given period from F to T',
    params: ['clientId', 'friendId'],
    schema: ['partyId', 'partyTitle', 'eventId', 'eventTitle', 'partyStartsAt']
  },
  {
    title: 'Find all days when the number of friends on vacation was between A and B (inclusive)',
    params: ['a', 'b'],
    period: false,
    schema: ['date', 'friendsOnVacation']
  },
  {
    title: 'Find average amount of complaint signs accusing friend X per month',
    params: ['friendId'],
    period: false,
    schema: ['month', 'complaints', 'averageComplaintSigns']
  }
];

const Param = def => (<input key={def.name} type={def.type} name={def.name} placeholder={def.placeholder} required/>);

const options = RQsDef.map((def, i) => ({value: i, label: `${i + 1}. ${def.title}`}));

const RequiredQueries = () => {
  const [RQIndex, setRQIndex] = useState(options[0]);
  const [data, setData] = useState([]);
  const [message, setMessage] = useState('Set parameters at the sidebar and click "Query" button!');

  const {title, params, period, schema} = RQsDef[RQIndex.value];

  const onSelectChange = option => {
    setRQIndex(option);
    setData([]);
  }

  const onSubmit = event => {
    event.preventDefault();
    const params = Object.fromEntries(new FormData(event.target));
    requiredQuery(RQIndex.value+1, params)
      .then(r => {
        const items = r.data.items;
        if(items.length > 0)
          setData(items);
        else
          setMessage('The query returned empty result');
      })
      .catch(e => setMessage(e.data.message))
  };

  return (
    <div className='required-queries'>
      <aside className='required-queries-filters-sidebar'>
        <h3>Parameters</h3>
        <form className='form' onSubmit={onSubmit}>
          {(period === false ? params : params.concat(['from', 'to'])).map(p => Param(paramsDef[p]))}
          <input type='submit' value='Query'/>
        </form>
      </aside>
      <div className='margined required-queries-body'>
        <Select options={options} value={RQIndex} onChange={onSelectChange}/>
        <h2>{title}</h2>
        {message && (<p>{message}</p>)}
        {!message && <Table schema={completeSchema(schema)} data={data}/>}
      </div>
    </div>
  );
}

export default RequiredQueries;
