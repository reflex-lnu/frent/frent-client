import '../style.css'
import {humanizeHoldingTime} from "../../utils";

const EventItem = ({event}) => {
  const {title, description, startsAt, endsAt, isRecurring} = event;

  return (
    <div className='list-item'>
      <h3>{title}</h3>
      <hr/>
      <p>{description}</p>
      <p>{humanizeHoldingTime(startsAt, endsAt)}</p>
      <p>{(isRecurring && 'Repeats every year') || 'Exclusive for this year'}</p>
    </div>
  )
}

export default EventItem;
