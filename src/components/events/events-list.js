import EventItem from "./event-item";
import {getAllEvents} from "../../api";
import '../style.css'
import {useState} from "react";

const EventsList = () => {
  const [events, setEvents] = useState([]);

  events.length > 0 || getAllEvents().then(items => {
    setEvents(items);
  });

  return (
    <div className='margined'>
      <h1 className='align-center'>Events</h1>
      <ul className='list'>
        {events.map(e => <li><EventItem key={e.id} event={e}/></li>)}
      </ul>
    </div>
  )
}

export default EventsList;
