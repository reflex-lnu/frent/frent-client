import '../style.css'
import {humanizeHoldingTime} from "../../utils";
import {getEventById} from "../../api";

const PartyItem = ({party}) => {
  const {eventId, title, description, startsAt, endsAt, address, createdAt} = party;

  return (
    <div className='list-item'>
      <h3>{title}</h3>
      <hr/>
      {eventId && <p>Related event: <strong>{getEventById(eventId).title}</strong></p>}
      <p>{description}</p>
      <p>{humanizeHoldingTime(startsAt, endsAt)}</p>
      <p>{address}</p>
      <p>(Created at {createdAt.toLocaleString('uk')})</p>
      <p></p>
    </div>
  )
}

export default PartyItem;