import PartyItem from "./party-item";
import {getAllParties} from "../../api";
import '../style.css'

const PartiesList = () => {
  const parties = getAllParties();

  const partyElements = parties.map(p => <li><PartyItem key={p.id} party={p}/></li>)

  return (
    <div className='margined'>
      <h1 className='align-center'>Parties</h1>
      <ul className='list'>
        {partyElements}
      </ul>
    </div>
  )
}

export default PartiesList;
