import {useState} from "react";
import {Link, useNavigate} from "react-router-dom";
import '../style.css'
import {signUp} from "../../api";


const SignUp = () => {
  const [failed, setFailed] = useState(null);
  const navigate = useNavigate();

  const onSubmit = event => {
    event.preventDefault();
    const {
      fullName, birthDate, role, sex, email,
      phoneNumber, address, password, passwordAgain
    } = Object.fromEntries(new FormData(event.target));
    if (password !== passwordAgain) {
      setFailed('Passwords don\'t match');
    } else {
      const user = {fullName,  birthDate, role, sex, email, phoneNumber, address, password}

      signUp(user).then(res => {
      if (res.status === 200)
        navigate('/events');
      else
        setFailed(true);
    }).catch(e => console.log(e));
    }
  };

  return (
    <div className='margined'>
      <h2>Create a new account!</h2>
      <form className='form' onSubmit={onSubmit}>
        {failed && <p className='login-failed'>{failed}. Try again</p>}
        <input type="text" placeholder="Full Name" name="fullName" required/>
        <input type="date" placeholder="Birth Date" name="birthDate" required/>
        <fieldset style={{display: "inline-flex"}}>
          <legend>Sex</legend>
          <input type="radio" id="male" name="sex" value="male" checked/>
          <label htmlFor="male">Male</label>
          <input type="radio" id="female" name="sex" value="female"/>
          <label htmlFor="Female">Female</label>
        </fieldset>
        <fieldset style={{display: "inline-flex"}}>
          <legend>Role</legend>
          <input type="radio" id="client" name="role" value="client" checked/>
          <label htmlFor="male">Client</label>
          <input type="radio" id="friend" name="role" value="friend"/>
          <label htmlFor="Female">Friend</label>
        </fieldset>
        <input type="email" placeholder="Email" name="email" required/>
        <input type="tel" pattern="[0-9]{7,15}" placeholder="Phone Number" name="phoneNumber" required/>
        <input type="text" placeholder="Address" name="address" required/>
        <input type="password" placeholder="Password" name="password" required/>
        <input type="password" placeholder="Repeat password" name="passwordAgain" required/>
        <input type="submit" value='Sign Up'/>
      </form>
      <Link to='/sign-in'>Already have an account? Sign in!</Link>
    </div>
  )
};

export default SignUp;
