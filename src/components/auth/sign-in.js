import {useState} from "react";
import {Link, useNavigate} from "react-router-dom";
import '../style.css'
import {signIn} from '../../api';
import {login} from "../../api";

const SignIn = () => {
  const [failed, setFailed] = useState(false);
  const navigate = useNavigate();


  const onSubmit = event => {
    event.preventDefault();
    const {email, password} = Object.fromEntries(new FormData(event.target));

    signIn(email, password)
      .then(r => {
        login(r.data.token);
        navigate('/events');
      })
      .catch(e => {
        console.log("Authorization failed");
        console.log(e);
        setFailed(true)
      });

  };

  return (
    <div className='margined'>
      <h2>Sign in into account!</h2>
      <form className='form' onSubmit={onSubmit}>
        {failed && <p className='login-failed'>Incorrect username or password. Try again</p>}
        <input type="text" placeholder="Email" name="email" required/>
        <input type="password" placeholder="Password" name="password" required/>
        <input type="submit" value='Sign In'/>
      </form>
      <Link to='/sign-up'>Don't have an account yet? Create one for free!</Link>
    </div>
  )
};

export default SignIn;
