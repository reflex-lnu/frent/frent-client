import {Link} from "react-router-dom";
import './header.css'
import {useAuth, logout} from "../api";

const Header = () => {
  const [logged] = useAuth();

  return (
    <header className='header'>
      <h1>Frent</h1>
      {logged && (
        <>
          <Link to='/events'><p>Events</p></Link>
          <Link to='/parties'><p>Parties</p></Link>
        </>
      )}
      <Link to='/required-queries'><p>Required Queries</p></Link>
      <div className='align-right'>
        {logged && (
          <Link to='/sign-in' onClick={_ => logout()}><p>Log out</p></Link>
        )}
        {!logged && (
          <Link to='/sign-in'><p>Sign In / Sign Up</p></Link>
        )}
      </div>
    </header>
  );
};

export default Header;
