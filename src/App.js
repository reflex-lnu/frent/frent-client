import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom";
import EventsList from "./components/events/events-list";
import SignIn from "./components/auth/sign-in";
import SignUp from "./components/auth/sign-up";
import PartiesList from "./components/parties/parties-list";
import AddParty from "./components/parties/add-party";
import Header from "./components/header";
import Footer from "./components/footer";
import RequiredQueries from "./components/required-queries/required-queries"
import {useAuth} from "./api";

function App() {
  const [logged] = useAuth();

  return (
    <BrowserRouter>
      <Header/>
      <Routes>
        {logged && (
          <>
            <Route path='/events' element={<EventsList/>}/>
            <Route path='/parties' element={<PartiesList/>}/>
            <Route path='/parties/add' element={<AddParty/>}/>
            <Route path='/' element={<Navigate to='/events'/>}/>
            <Route path='*' element={<h1>Not Found</h1>}/>
          </>
        )}
        {!logged && (
          <>
            <Route path='/sign-in' element={<SignIn/>}/>
            <Route path='/sign-up' element={<SignUp/>}/>
            <Route path='*' element={<Navigate to='/sign-in'/>}/>
          </>
        )}
        <Route path='/required-queries' element={<RequiredQueries/>}/>
      </Routes>
      <Footer/>
    </BrowserRouter>
  );
}

export default App;
