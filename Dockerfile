FROM node:19-alpine AS builder

WORKDIR /frent-client

COPY package.json package-lock.json ./

RUN npm ci

COPY public public
COPY src src

RUN npm run build



FROM node:19-alpine

COPY --from=builder /frent-client/build /frent-client

CMD ["npx", "serve", "/frent-client"]
