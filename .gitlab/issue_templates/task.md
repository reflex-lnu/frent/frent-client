## Preface
_Опційна секція. Загальні відомості і положення для кращого розуміння завдання_

## Subject
_Опис завдання, в чому воно полягає, його кроків/етапів_

## Motivation
_Опційна секція. Що наштовхнуло на це завдання, яка користь, які проблеми вирішує_

## Challenges
_Опційна секція. Труднощі і проблеми, які це завдання може створити, і попередні пропозиції щодо їх подолання/уникнення/обходу_

## References
_Опційна секція. Посилання на пов'язані статті, відео, документації й інші ресурси_
